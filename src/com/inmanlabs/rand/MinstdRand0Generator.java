package com.inmanlabs.rand;

/**
 * The MinstdRand0Generator is a ParkMillerGenerator with a multiplier of 16807
 */
@Deprecated // in favor of MinstdRandGenerator
public class MinstdRand0Generator extends ParkMillerGenerator {
    private static long MINSTD0_MULTIPLIER = 16807;

    public MinstdRand0Generator(long seed) {
        super(MinstdRand0Generator.MINSTD0_MULTIPLIER, seed);
    }
}
