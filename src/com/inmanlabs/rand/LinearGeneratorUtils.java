package com.inmanlabs.rand;

import java.security.InvalidParameterException;

/**
 * Static methods for Linear Congruential Generators
 */
public class LinearGeneratorUtils {

    /**
     * generates a pseudorandom value given the three inputs and using the park-miller generator
     *
     * @param m modulus
     * @param a multiplier
     * @param x seed/"start value"
     * @return
     */
    public static long parkMiller(long m, long a, long x) {
        return lcg(m, a, 0, x);
    }

    /**
     * generates a pseudorandom value given the four inputs using the linear congruential generator
     *
     * @param m modulus
     * @param a multiplier
     * @param c increment
     * @param x seed/"start value"
     * @return
     */
    public static long lcg(long m, long a, long c, long x) {
        LinearGeneratorUtils.validateParameters(m, a, c, x);
        return (a * (x + c)) % m;
    }

    public static void validateParameters(long modulus, long multiplier, long increment, long seed) {
        if (modulus <= 0) {
            throw new InvalidParameterException("the 'modulus' of a linear congruential generator must be positive");
        }

        if (multiplier <= 0) {
            throw new InvalidParameterException("the 'multiplier' of a linear congruential generator must be positive");
        } else if (multiplier >= modulus) {
            throw new InvalidParameterException(String.format(
                    "the 'multiplier' of a linear congruential generator must be less than the 'modulus' (%s)", modulus
            ));
        }

        if (increment < 0) {
            throw new InvalidParameterException("the 'increment' of a linear congruential generator must be non-negative");
        } else if (increment >= modulus) {
            throw new InvalidParameterException(String.format(
                    "the 'increment' of a linear congruential generator must be less than the 'modulus' (%s)", modulus
            ));
        }

        if (seed < 0) {
            throw new InvalidParameterException("the 'seed' of the linear congruential generator must be non-negative");
        } else if (seed >= modulus) {
            throw new InvalidParameterException(String.format(
                    "the 'seed' of a linear congruential generator must be less than the 'modulus' (%s)", modulus
            ));
        }
    }
}
