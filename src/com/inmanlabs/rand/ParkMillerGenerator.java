package com.inmanlabs.rand;

/**
 * The ParkMillerGenerator is an abstract class to support an implementation of a Park-Miller pseudorandom number generator.
 * Given a seed and a multiplier it will continuously generate new pseudorandom numbers each time the next() method is invoked.
 * <p>
 * An implementing class is required to specify the multiplier. You should pick an integer with high multiplicative order!
 */
public abstract class ParkMillerGenerator {
    private static long MODULUS = (long) Math.pow(2, 31) - 1; // 2147483647

    private final long multiplier;
    private long currentValue;

    public ParkMillerGenerator(long multiplier, long seed) {
        this.multiplier = multiplier;
        this.currentValue = seed;
    }

    /**
     * Generates and returns the next pseudorandom number
     *
     * @return
     */
    public long next() {
        this.currentValue = LinearGeneratorUtils.parkMiller(ParkMillerGenerator.MODULUS, this.multiplier, this.currentValue);
        return this.currentValue;
    }
}
