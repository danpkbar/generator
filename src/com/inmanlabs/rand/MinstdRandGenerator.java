package com.inmanlabs.rand;

/**
 * The MinstdRandGenerator is a ParkMillerGenerator with a multiplier of 48271
 */
public class MinstdRandGenerator extends ParkMillerGenerator {
    private static long MISNSTD_MULTIPLIER = 48271;

    public MinstdRandGenerator(long seed) {
        super(MinstdRandGenerator.MISNSTD_MULTIPLIER, seed);
    }
}
