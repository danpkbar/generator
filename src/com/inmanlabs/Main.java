package com.inmanlabs;

import com.inmanlabs.rand.MinstdRand0Generator;
import com.inmanlabs.rand.MinstdRandGenerator;
import com.inmanlabs.bits.BitUtils;

import java.time.Duration;
import java.time.Instant;

public class Main {

    public static void main(String[] args) {
        Instant start = Instant.now();

        long seedA = 65;
        long seedB = 8921;

        MinstdRand0Generator generatorA = new MinstdRand0Generator(seedA);
        MinstdRandGenerator generatorB = new MinstdRandGenerator(seedB);

        int n = 40000000;
        int difficulty = 16;
        long bitmask = BitUtils.filledBitmaskOfLength(difficulty);

        int matches = 0;
        for (int i = 0; i < n; i++) {
            long nextRandomA = generatorA.next();
            long nextRandomB = generatorB.next();

            if (BitUtils.bitwiseMatch(nextRandomA, nextRandomB, bitmask)) {
                matches++;
            }
        }

        Instant end = Instant.now();
        System.out.println(String.format("matches: %s, duration: %s", matches, Duration.between(start, end)));
    }
}
