package com.inmanlabs.bits;

/**
 * Static methods useful for bit manipulation
 *
 * @author danbarrett
 */
public class BitUtils {
    /**
     * Checks to see if a and b are equal after being mashed up against the given bitmask
     * <p>
     * Put another way, this method checks to see if the bytes declared by the bitmask are equal across a and b.
     * <p>
     * ie. a bitmask of 0b0001 will check to see if the least-significant-bit of both a and b are equal.
     * Similarly, a bitmask of 0b0101 will check to see if the least-significant-bit AND the third-least-significant-bit of both a and b are equal.
     *
     * @param bitmask
     * @param a
     * @param b
     * @return
     */
    public static boolean bitwiseMatch(long a, long b, long bitmask) {
        return ((a ^ b) & bitmask) == 0;
    }

    /**
     * Creates a fully filled bitmask for the given number of bits
     * <p>
     * ie. 16 will return 0b1111111111111111 which is a bitmask of (2^16)-1
     *
     * @param numberOfBits
     * @return
     */
    public static long filledBitmaskOfLength(int numberOfBits) {
        if (numberOfBits < 0) {
            throw new RuntimeException("A bitmask of negative length is nonsensical");
        }
        return (long) Math.pow(2, numberOfBits) - 1;
    }
}
